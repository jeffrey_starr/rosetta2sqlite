#!/usr/bin/env python3

import argparse
import dataclasses
import datetime
import itertools
import pathlib
import re
import sqlite3
import typing
import unittest
from xml.etree import ElementTree as ETree


@dataclasses.dataclass
class Task:
    """
    A programming task that may be accomplished in multiple languages
    """
    id: int
    last_updated: datetime.datetime
    title: str
    content: str
    description: typing.Optional[str] = None

    @classmethod
    def schema(cls) -> str:
        return """
        CREATE TABLE task (
            id INTEGER PRIMARY KEY,
            last_updated DATETIME NOT NULL,
            title TEXT NOT NULL,
            content TEXT NOT NULL,
            description TEXT
        )
        """


@dataclasses.dataclass(frozen=True)
class Language:
    """
    A programming language that may be used to complete Tasks
    """
    id: int
    key: str

    @classmethod
    def schema(cls) -> str:
        return """
        CREATE TABLE language (
            id INTEGER PRIMARY KEY,
            key TEXT UNIQUE
        )
        """


@dataclasses.dataclass
class Submission:
    """
    Code submitted to accomplish some Task in some Language
    """
    id: int
    task: int
    language: int
    description: str
    code: str
    output: str

    @classmethod
    def schema(cls) -> str:
        return """
        CREATE TABLE submission (
            id INTEGER PRIMARY KEY,
            task REFERENCES task (id),
            language REFERENCES language (id),
            description TEXT NOT NULL,
            code TEXT NOT NULL,
            output TEXT NOT NULL
        )
        """


@dataclasses.dataclass
class SubmissionIndex:
    """
    An index entry storing the relevant string positions within task.content that match a given language
    entry. A SubmissionIndex entry may yield multiple Submissions as the text_start to text_end are indices
    of a language entry, and the text may contain multiple submissions. Note that SubmissionIndex does not
    include a reference to the Language, as headers are unreliable and we use the <lang key> as the authority.
    """
    task: int
    text_start: int  # inclusive position
    text_end: int  # exclusive position


def prepare(sqlite_uri: str) -> sqlite3.Connection:
    """
    Open a connection to the sqlite file and prepare the database schema for later loading.

    :param sqlite_uri: a URI pointing to the sqlite file (or pseudo-file)
    :return:
    """
    conn = sqlite3.connect(sqlite_uri)
    with conn:
        conn.execute(Task.schema())
        conn.execute(Language.schema())
        conn.execute(Submission.schema())

    return conn


def extract(xml_content: str) -> typing.List[Task]:
    """
    From the xml_file, extract the raw wikitext content for each task

    :param xml_content: string containing the exported XML data
    :return:
    """
    root = ETree.fromstring(xml_content)
    ns = "{http://www.mediawiki.org/xml/export-0.10/}"
    tasks = []

    for page in root.findall(f'{ns}page'):
        id_ = int(page.find(f'{ns}id').text)
        last_updated_raw = page.find(f'{ns}revision/{ns}timestamp').text.strip()
        # python's `fromisoformat` does not support named time zones, so we need to switch the UTC code 'Z' to
        # an explicit timezone offset
        if last_updated_raw.endswith('Z'):
            last_updated_raw = last_updated_raw[:-1] + '+00:00'
        last_updated = datetime.datetime.fromisoformat(last_updated_raw)
        title = page.find(f'{ns}title').text.strip()
        content = page.find(f'{ns}revision[1]/{ns}text').text.strip()
        tasks.append(Task(id_, last_updated, title, content, None))

    return tasks


def transform1(tasks: typing.List[Task]) -> (typing.List[Task], typing.List[SubmissionIndex]):
    """
    From each task, extract the description from the task content (returning a new task list)
    and extract blocks of text (that nominally are for a single language) for further processing.

    :param tasks:
    :return:
    """
    tasks_with_defs: typing.List[Task] = []
    index: typing.List[SubmissionIndex] = []

    language_header_regex = re.compile(r"=={{header\|(?P<langname>.+?)}}(\s*(/|and)\s*{{header\|(?P<altname>.+?)}})?==\s+", re.RegexFlag.I)

    for task in tasks:
        language_headers: typing.List[re.Match] = list(language_header_regex.finditer(task.content))

        if language_headers is None:
            print(f'Ignoring task {task.id}/{task.title} as there appears to be no submissions')
        else:
            if language_headers:
                description = task.content[0:language_headers[0].start()].strip()
                tasks_with_defs.append(dataclasses.replace(task, description=description))

            for header, next_header in itertools.zip_longest(language_headers, language_headers[1:]):
                if next_header:
                    next_header_start = next_header.start()
                else:
                    next_header_start = len(task.content)

                index.append(SubmissionIndex(task.id, header.end(), next_header_start))

    index.sort(key=dataclasses.astuple)

    return tasks_with_defs, index


def print_progress(completed: int, total: int):
    """
    Periodically indicate progress

    :param completed: work units completed
    :param total: total work units expected
    :return:
    """
    if completed > 0 and completed % 1000 == 0:
        print(f'Completed {completed/total:.2%}')


def transform2(tasks: typing.List[Task], index: typing.List[SubmissionIndex]) -> (typing.List[Language], typing.List[Submission]):
    """
    From each task, extract the individual submissions.

    Via profiling, this is where the vast majority of time is spent.

    :param tasks:
    :param languages:
    :param index:
    :return:
    """
    submissions: typing.List[Submission] = []
    next_submission_id: int = 0
    tasks_dict = {t.id: t for t in tasks}
    languages_dict: typing.Dict[str, int] = {}
    next_language_id: int = 0
    submission_re = re.compile(r'(?P<submissions>(?P<submission>(?P<description>.*?)\s*<lang (?P<langkey>[a-zA-Z_:][a-zA-Z0-9_: \-]*)>(?P<code>.+?)</lang>)+?)', re.RegexFlag.I | re.RegexFlag.S)
    output_re = re.compile(r'{{Out}}\s*<pre>(?P<output>.+?)</pre>', re.RegexFlag.I | re.RegexFlag.S)
    total_work = len(index)

    for entry_idx, entry in enumerate(index):
        print_progress(entry_idx, total_work)
        # An entry (from entry.text_start to entry.text_end) is made up of:
        # (DESCRIPTION? <lang language.name>CODE</lang> ({{Out}} <pre>OUTPUT</pre>)?)+
        # where each instance is a submission
        task = tasks_dict[entry.task]

        submission_matches: typing.List[re.Match] = list(submission_re.finditer(task.content, entry.text_start, entry.text_end))

        for submission_match, next_submission_match in itertools.zip_longest(submission_matches, submission_matches[1:]):
            language_key = submission_match.group('langkey').lower()
            description = submission_match.group('description').strip()
            code = submission_match.group('code').strip()
            if next_submission_match is None:
                submission_end = entry.text_end
            else:
                submission_end = next_submission_match.end('description')
            output_s = output_re.search(task.content, submission_match.end('code') + len('</lang>'), submission_end)
            if output_s:
                output = output_s.group('output')
            else:
                output = ''

            if language_key in languages_dict:
                language_id = languages_dict[language_key]
            else:
                languages_dict[language_key] = next_language_id
                language_id = next_language_id
                next_language_id = next_language_id + 1

            submissions.append(Submission(next_submission_id, task.id, language_id, description, code, output))
            next_submission_id = next_submission_id + 1

    languages: typing.List[Language] = []
    for key, id in languages_dict.items():
        languages.append(Language(id, key))
    languages.sort(key=lambda l: l.key)

    return languages, submissions


def load(db: sqlite3.Connection,
         tasks: typing.List[Task],
         languages: typing.List[Language],
         submissions: typing.List[Submission]) -> sqlite3.Connection:
    """

    :param db:
    :param tasks:
    :param languages:
    :param submissions:
    :return:
    """
    with db:
        db.executemany('INSERT INTO task VALUES (?, ?, ?, ?, ?)', [dataclasses.astuple(t) for t in tasks])
    with db:
        db.executemany('INSERT INTO language VALUES (?, ?)', [dataclasses.astuple(l) for l in languages])
    with db:
        db.executemany('INSERT INTO submission VALUES (?, ?, ?, ?, ?, ?)', [dataclasses.astuple(s) for s in submissions])
    return db


class ExtractTests(unittest.TestCase):
    def test_extract_basic_page(self):
        xml = """
        <mediawiki xmlns="http://www.mediawiki.org/xml/export-0.10/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.10/ http://www.mediawiki.org/xml/export-0.10.xsd" version="0.10" xml:lang="en">
            <page>
                <title>Title</title>
                <id>5005</id>
                <revision>
                    <id>Numeric identifier</id>
                    <timestamp>2022-04-11T23:41:09Z</timestamp>
                    <model>wikitext</model>
                    <format>text/x-wiki</format>
                    <text>
                        Textual content
                    </text>
                </revision>
            </page>
        </mediawiki>
        """
        task = extract(xml)[0]

        self.assertEqual(task.id, 5005)
        self.assertEqual(task.title, 'Title')
        self.assertEqual(task.last_updated, datetime.datetime(2022, 4, 11, 23, 41, 9, 0, datetime.timezone.utc))
        self.assertEqual(task.content, 'Textual content')
        self.assertIsNone(task.description)


class LoadTests(unittest.TestCase):
    def setUp(self):
        self.date = datetime.datetime.utcnow()

    def sample_fixture(self) -> (typing.List[Task], typing.List[Language], typing.List[Submission]):
        tasks = [
            Task(1, self.date, 'Task 1', 'Task 1 content', 'Task 1 description'),
            Task(2, self.date, 'Task 2', 'Task 2 content', 'Task 2 description')
        ]
        languages = [
            Language(1, 'language1'),
            Language(2, 'language2'),
            Language(3, 'language3')
        ]
        submissions = [
            Submission(1, 1, 1, 'Std', 'def task1 in language 1', ''),
            Submission(2, 2, 1, 'Foo', 'def task2 in language 1', 'output'),
            Submission(3, 2, 3, 'Bar', 'def task2 in language 3', 'out')
        ]

        return tasks, languages, submissions

    def test_load(self):
        uri = ':memory:'
        # unfortunately, load depends on a schema being created, so this test also tests the prepare function
        conn = prepare(uri)
        tasks, languages, submissions = self.sample_fixture()
        conn = load(conn, tasks, languages, submissions)
        try:
            for (table, cnt) in [('task', 2), ('language', 3), ('submission', 3)]:
                self.assertEqual(conn.execute(
                    f"SELECT COUNT(*) FROM {table}").fetchone()[0],
                    cnt,
                    f"Counts do not agree for table {table}"
                )
        finally:
            conn.close()


class PrepareTests(unittest.TestCase):
    def test_prepare_creates_tables(self):
        uri = ':memory:'
        conn = prepare(uri)
        try:
            for table in ['task', 'language', 'submission']:
                self.assertEqual(conn.execute(
                    f"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='{table}'").fetchone()[0],
                    1,
                    f"Table {table} does not exist")
        finally:
            conn.close()


class Transform1and2Tests(unittest.TestCase):
    @staticmethod
    def wrap_content(content: str, id: int = 1) -> Task:
        return Task(id, datetime.datetime.now(), 'Task', content, description=None)

    def test_transform_no_submissions(self):
        # A Task with no submissions is dropped from the list
        content = """
        This is a great task with submissions I will add later
        
        """
        task = Transform1and2Tests.wrap_content(content)
        tasks, index = transform1([task])
        self.assertFalse(tasks)
        self.assertFalse(index)
        languages, submissions = transform2(tasks, index)
        self.assertFalse(languages)
        self.assertFalse(submissions)

    def test_transform_empty_description(self):
        content = """
        =={{header|Groovy}}==
        Start with smallest terms first to minimize rounding error:
        <lang groovy>println ((1000..1).collect { x -&gt; 1/(x*x) }.sum())</lang>;
        """
        task = Transform1and2Tests.wrap_content(content)
        tasks, index = transform1([task])
        self.assertEqual(tasks[0].description, '')
        self.assertEqual(index, [SubmissionIndex(1, content.index('Start with smallest terms'), len(content))])
        languages, submissions = transform2(tasks, index)
        self.assertEqual(languages, [
            Language(0, 'groovy')
        ])
        self.assertEqual(submissions, [
            Submission(0, task.id, languages[0].id,
                       'Start with smallest terms first to minimize rounding error:',
                       'println ((1000..1).collect { x -&gt; 1/(x*x) }.sum())',
                       '')
        ])

    def test_transform_with_submissions(self):
        content = """
        Task description
        =={{header|JavaScript}}==
        ===ES5===
        ====Iterative====
        
        Does not check for duplicate indices.
        <lang JavaScript>function sort_disjoint(values, indices) {
          var sublist = [];
          indices.sort(function(a, b) { return a &gt; b; });
          ...
        }</lang>

        ====Functional====

        <lang JavaScript>(function () {
            ...
        })()</lang>

        {{Out}}
        <pre>[7, 0, 5, 4, 3, 2, 1, 6]</pre>;

        ===ES6===

        <lang JavaScript>(() =>; {
          'use strict';
            ...
          // MAIN ---
          return main();
        })()</lang>
        {{Out}}
        <pre>[7, 0, 5, 4, 3, 2, 1, 6]</pre>
        
        =={{header|jq}}==

        We define a jq function, disjointSort, that accepts the array of values as input,
        but for clarity we first define a utility function for updating an array at multiple places:
        <lang jq>;def setpaths(indices; values):
          reduce range(0; indices|length) as $i
            (.; .[indices[$i]] = values[$i]);
        </lang>
        """
        task = Transform1and2Tests.wrap_content(content)
        tasks, index = transform1([task])
        self.assertEqual(tasks[0].description, 'Task description')
        # NB: A SubmissionIndex is an index around language editions; it may encompass multiple Submissions
        self.assertEqual(index, [
            SubmissionIndex(1, content.index('===ES5==='), content.index('=={{header|jq}}==')),
            SubmissionIndex(1, content.index('We define a jq function'), len(content))
        ])
        languages, submissions = transform2(tasks, index)
        self.assertEqual(languages, [
            Language(0, 'javascript'),
            Language(1, 'jq')
        ])
        self.maxDiff = None
        self.assertEqual(submissions[0],
            Submission(0, task.id, languages[0].id,
                       '===ES5===\n        ====Iterative====\n        \n        Does not check for duplicate indices.',
                       'function sort_disjoint(values, indices) {\n          var sublist = [];\n          indices.sort(function(a, b) { return a &gt; b; });\n          ...\n        }',
                       ''))
        self.assertEqual(submissions[1],
            Submission(1, task.id, languages[0].id,
                       '====Functional====',
                       '(function () {\n            ...\n        })()',
                       '[7, 0, 5, 4, 3, 2, 1, 6]'))
        self.assertEqual(submissions[2],
            Submission(2, task.id, languages[0].id,
                       '{{Out}}\n        <pre>[7, 0, 5, 4, 3, 2, 1, 6]</pre>;\n\n        ===ES6===',
                       '(() =>; {\n          \'use strict\';\n            ...\n          // MAIN ---\n          return main();\n        })()',
                       '[7, 0, 5, 4, 3, 2, 1, 6]'))
        self.assertEqual(submissions[3],
            Submission(3, task.id, languages[1].id,
                       'We define a jq function, disjointSort, that accepts the array of values as input,\n        but for clarity we first define a utility function for updating an array at multiple places:',
                       ';def setpaths(indices; values):\n          reduce range(0; indices|length) as $i\n            (.; .[indices[$i]] = values[$i]);',
                       ''))

    def test_multiple_tasks_intersecting_languages(self):
        content1 = """
        Task description
        =={{header|JavaScript}}==
        <lang JavaScript>Hello, world</lang>
        
        =={{Header|Lua}}==
        <lang Lua>Hello, world</lang>        
        """
        content2 = """
        A different task description
        =={{header|JavaScript}}==
        <lang JavaScript>Hello, universe</lang>
        
        =={{Header|Lua}}==
        <lang Lua>Hello, universe</lang>        
        """
        tasks = [self.wrap_content(content1, id=1), self.wrap_content(content2, id=2)]
        tasks, index = transform1(tasks)
        self.assertEqual(tasks[0].description, 'Task description')
        self.assertEqual(tasks[1].description, 'A different task description')
        languages, _ = transform2(tasks, index)
        self.assertEqual(len(languages), 2)
        self.assertEqual(languages, [
            Language(0, 'javascript'),
            Language(1, 'lua')
        ])

    def test_alternate_names(self):
        content = """
        =={{header|Mathematica}} / {{header|Wolfram Language}}==
        <lang Mathematica>StringTake["000000" <> ToString[7.125], -9]
        00007.125</lang>
        
        =={{header|Delphi}} and {{header|Pascal}}==
        <lang Delphi>f</lang>
        """
        tasks = [self.wrap_content(content)]
        tasks, index = transform1(tasks)
        languages, _ = transform2(tasks, index)
        self.assertEqual(languages, [
            Language(1, 'delphi'),
            Language(0, 'mathematica')
        ])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert a Rosetta Code export to a sqlite file')
    parser.add_argument('xml', type=pathlib.Path, help='RosettaCode xml export file')
    parser.add_argument('sql', type=pathlib.Path, help='sqlite output file')
    args = parser.parse_args()

    with open(args.xml, 'r') as xml_file:
        xml_content = xml_file.read()
        with prepare(pathlib.Path(args.sql).absolute().as_uri()) as conn:
            tasks = extract(xml_content)
            tasks, index = transform1(tasks)
            languages, submissions = transform2(tasks, index)
            load(conn, tasks, languages, submissions)
