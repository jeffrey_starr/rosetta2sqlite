# rosetta2sqlite

Convert a [Rosetta Code](https://rosettacode.org) export to a sqlite file

This is a Python 3.8+ script with no external dependencies.

## Usage

`$ python3 -m rosetta2sqlite rosetta-export.xml out.sqlite`

To run tests:

`$ python3 -m unittest rosetta2sqlite`

To run the module with a profiler:

`$python3 -m cProfile -o ten-percent.prof -m rosetta2sqlite Rosetta-ten-percent.xml code-10.sqlite`

## Schemas

### Output File (sqlite)

|          | Task          |
|----------|---------------|
| PK       | id            |
| datetime | last_updated  |
| string   | title         |
| string   | content       |
| string   | description   |

The Task id is set to the XML's page/id.

The `content` is the task's latest `revision` `text` content.

The `description` is the subset of the `content` from the Task start to the first
submission and should describe the expectations of the programming task.

|               | Language |
|---------------|----------|
| PK            | id       |
| string unique | key      |

The `key` of a Language is the attribute name used in the <lang key>...</lang>
submissions.

The Language is an auto-incremented id.

|                 | Submission      |
|-----------------|-----------------|
| PK              | id              |
| FK              | task            |
| FK              | language        |
| string          | description     |
| string          | code            |
| string          | output          |

There may be more than one Submission per task and language pair.

The `description` is the textual content prior to the source code.

The `code` is the textual content between <lang foo> and </lang foo>. (Note that the 
greater and less than signs will be escaped in the raw XML file.) The code will
be processed to eliminate any wikitext or other escaped characters; the intent is that
this string could be given to a compiler of the language without alteration.

The `output` is the optional textual content after the lang section that represents
the program's output. This is not always represented in the task description and there
is inconsistent formatting in the XML.

### Input File (xml and wikitext)

The mediawiki XML follows the [schema](https://www.mediawiki.org/xml/export-0.10.xsd). A simpler
description is:

```xml
<mediawiki>
    <page>
        <title>Title</title>
        <id>Numeric identifier</id>
        <revision>
            <id>Numeric identifier</id>
            <timestamp>2022-04-11T23:41:09Z</timestamp>
            <model>wikitext</model>
            <format>text/x-wiki</format>
            <text xml:space="preserve" bytes="90905">
                ...
            </text>
        </revision>
    </page>
</mediawiki>
```

If the export was limited to the latest revision, there will only be a single `revision` child
per `page`. If the export contained multiple tasks, then there will be multiple `page` children,
one per task.

Some examples of wikitext:

```text
=={{header|Groovy}}==
Start with smallest terms first to minimize rounding error:
&lt;lang groovy&gt;println ((1000..1).collect { x -&gt; 1/(x*x) }.sum())&lt;/lang&gt;

Output:
&lt;pre&gt;1.6439345654&lt;/pre&gt;

=={{header|Haskell}}==
With a list comprehension:
&lt;lang haskell&gt;sum [1 / x ^ 2 | x &lt;- [1..1000]]&lt;/lang&gt;
With higher-order functions:
&lt;lang haskell&gt;sum $ map (\x -&gt; 1 / x ^ 2) [1..1000]&lt;/lang&gt;
In [http://haskell.org/haskellwiki/Pointfree point-free] style:
&lt;lang haskell&gt;(sum . map (1/) . map (^2)) [1..1000]&lt;/lang&gt;
or
&lt;lang haskell&gt;(sum . map ((1 /) . (^ 2))) [1 .. 1000]&lt;/lang&gt;

or, as a single fold:

&lt;lang haskell&gt;seriesSum f = foldr ((+) . f) 0

inverseSquare = (1 /) . (^ 2)

main :: IO ()
main = print $ seriesSum inverseSquare [1 .. 1000]&lt;/lang&gt;
{{Out}}
&lt;pre&gt;1.6439345666815615&lt;/pre&gt;

=={{header|JavaScript}}==
===ES5===
&lt;lang javascript&gt;function sum(a,b,fn) {
   var s = 0;
   for ( ; a &lt;= b; a++) s += fn(a);
   return s;
}
 
 sum(1,1000, function(x) { return 1/(x*x) } )  // 1.64393456668156&lt;/lang&gt;

or, in a functional idiom:

&lt;lang JavaScript&gt;(function () {

  function sum(fn, lstRange) {
    return lstRange.reduce(
      function (lngSum, x) {
        return lngSum + fn(x);
      }, 0
    );
  }

  function range(m, n) {
    return Array.apply(null, Array(n - m + 1)).map(function (x, i) {
      return m + i;
    });
  }


  return sum(
    function (x) {
      return 1 / (x * x);
    },
    range(1, 1000)
  );

})();&lt;/lang&gt;

{{Out}}

&lt;lang JavaScript&gt;1.6439345666815615&lt;/lang&gt;

===ES6===
{{Trans|Haskell}}
&lt;lang JavaScript&gt;(() =&gt; {
    'use strict';

    // SUM OF A SERIES -------------------------------------------------------

    // seriesSum :: Num a =&gt; (a -&gt; a) -&gt; [a] -&gt; a
    const seriesSum = (f, xs) =&gt;
        foldl((a, x) =&gt; a + f(x), 0, xs);


    // GENERIC ---------------------------------------------------------------

    // enumFromToInt :: Int -&gt; Int -&gt; [Int]
    const enumFromTo = (m, n) =&gt;
        Array.from({
            length: Math.floor(n - m) + 1
        }, (_, i) =&gt; m + i);

    // foldl :: (b -&gt; a -&gt; b) -&gt; b -&gt; [a] -&gt; b
    const foldl = (f, a, xs) =&gt; xs.reduce(f, a);

    // TEST ------------------------------------------------------------------

    return seriesSum(x =&gt; 1 / (x * x), enumFromTo(1, 1000));
})();&lt;/lang&gt;
{{Out}}
&lt;lang JavaScript&gt;1.6439345666815615&lt;/lang&gt;

```